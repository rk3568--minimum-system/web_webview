/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MMI_KEYEVENT_ADAPTER_H
#define MMI_KEYEVENT_ADAPTER_H

#include <cstdint>

namespace OHOS::NWeb {
namespace MMIAdapter {
class KeyEvent {
public:
    static const int32_t KEYCODE_0;
    static const int32_t KEYCODE_1;
    static const int32_t KEYCODE_2;
    static const int32_t KEYCODE_3;
    static const int32_t KEYCODE_4;
    static const int32_t KEYCODE_5;
    static const int32_t KEYCODE_6;
    static const int32_t KEYCODE_7;
    static const int32_t KEYCODE_8;
    static const int32_t KEYCODE_9;
    static const int32_t KEYCODE_DPAD_UP;
    static const int32_t KEYCODE_DPAD_DOWN;
    static const int32_t KEYCODE_DPAD_LEFT;
    static const int32_t KEYCODE_DPAD_RIGHT;
    static const int32_t KEYCODE_A;
    static const int32_t KEYCODE_B;
    static const int32_t KEYCODE_C;
    static const int32_t KEYCODE_D;
    static const int32_t KEYCODE_E;
    static const int32_t KEYCODE_F;
    static const int32_t KEYCODE_G;
    static const int32_t KEYCODE_H;
    static const int32_t KEYCODE_I;
    static const int32_t KEYCODE_J;
    static const int32_t KEYCODE_K;
    static const int32_t KEYCODE_L;
    static const int32_t KEYCODE_M;
    static const int32_t KEYCODE_N;
    static const int32_t KEYCODE_O;
    static const int32_t KEYCODE_P;
    static const int32_t KEYCODE_Q;
    static const int32_t KEYCODE_R;
    static const int32_t KEYCODE_S;
    static const int32_t KEYCODE_T;
    static const int32_t KEYCODE_U;
    static const int32_t KEYCODE_V;
    static const int32_t KEYCODE_W;
    static const int32_t KEYCODE_X;
    static const int32_t KEYCODE_Y;
    static const int32_t KEYCODE_Z;
    static const int32_t KEYCODE_COMMA;
    static const int32_t KEYCODE_PERIOD;
    static const int32_t KEYCODE_ALT_LEFT;
    static const int32_t KEYCODE_ALT_RIGHT;
    static const int32_t KEYCODE_SHIFT_LEFT;
    static const int32_t KEYCODE_SHIFT_RIGHT;
    static const int32_t KEYCODE_TAB;
    static const int32_t KEYCODE_SPACE;
    static const int32_t KEYCODE_ENTER;
    static const int32_t KEYCODE_DEL;
    static const int32_t KEYCODE_GRAVE;
    static const int32_t KEYCODE_MINUS;
    static const int32_t KEYCODE_EQUALS;
    static const int32_t KEYCODE_LEFT_BRACKET;
    static const int32_t KEYCODE_RIGHT_BRACKET;
    static const int32_t KEYCODE_BACKSLASH;
    static const int32_t KEYCODE_SEMICOLON;
    static const int32_t KEYCODE_APOSTROPHE;
    static const int32_t KEYCODE_SLASH;
    static const int32_t KEYCODE_PAGE_UP;
    static const int32_t KEYCODE_PAGE_DOWN;
    static const int32_t KEYCODE_ESCAPE;
    static const int32_t KEYCODE_FORWARD_DEL;
    static const int32_t KEYCODE_CTRL_LEFT;
    static const int32_t KEYCODE_CTRL_RIGHT;
    static const int32_t KEYCODE_CAPS_LOCK;
    static const int32_t KEYCODE_SCROLL_LOCK;
    static const int32_t KEYCODE_META_LEFT;
    static const int32_t KEYCODE_META_RIGHT;
    static const int32_t KEYCODE_SYSRQ;
    static const int32_t KEYCODE_BREAK;
    static const int32_t KEYCODE_MOVE_HOME;
    static const int32_t KEYCODE_MOVE_END;
    static const int32_t KEYCODE_INSERT;
    static const int32_t KEYCODE_F1;
    static const int32_t KEYCODE_F2;
    static const int32_t KEYCODE_F3;
    static const int32_t KEYCODE_F4;
    static const int32_t KEYCODE_F5;
    static const int32_t KEYCODE_F6;
    static const int32_t KEYCODE_F7;
    static const int32_t KEYCODE_F8;
    static const int32_t KEYCODE_F9;
    static const int32_t KEYCODE_F10;
    static const int32_t KEYCODE_F11;
    static const int32_t KEYCODE_F12;
    static const int32_t KEYCODE_NUM_LOCK;
    static const int32_t KEYCODE_NUMPAD_0;
    static const int32_t KEYCODE_NUMPAD_1;
    static const int32_t KEYCODE_NUMPAD_2;
    static const int32_t KEYCODE_NUMPAD_3;
    static const int32_t KEYCODE_NUMPAD_4;
    static const int32_t KEYCODE_NUMPAD_5;
    static const int32_t KEYCODE_NUMPAD_6;
    static const int32_t KEYCODE_NUMPAD_7;
    static const int32_t KEYCODE_NUMPAD_8;
    static const int32_t KEYCODE_NUMPAD_9;
    static const int32_t KEYCODE_NUMPAD_DIVIDE;
    static const int32_t KEYCODE_NUMPAD_MULTIPLY;
    static const int32_t KEYCODE_NUMPAD_SUBTRACT;
    static const int32_t KEYCODE_NUMPAD_ADD;
    static const int32_t KEYCODE_NUMPAD_DOT;
    static const int32_t KEYCODE_NUMPAD_ENTER;
};
} // namespace MMIAdapter
} // namespace OHOS::NWeb

#endif // MMI_KEYEVENT_ADAPTER_H
